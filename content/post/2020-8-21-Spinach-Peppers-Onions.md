---
title: Pizza 1
subtitle: Spinach, Peppers, and Onions
date: 2020-08-21
tags: ["spinach", "peppers", "onions"]
---

## Crust
* 1/2 cup water
* 2.5 tsp yeast
* 1 cup flour, plus more as needed
* 6 tsp sugar
* salt
* olive oil
* dried basil
* garlic powder
* oregano

Added 1 cup of flour, a dash of salt, and spices to the bowl of a stand mixer. Measured warm water and added 3 tsp sugar, and yeast. Mixed water, sugar, and yeast with fork until fully disolved. Added water mixture and oil to dry ingredients and mixed with mixer on medium. On tasting mixture added 3 more tsp sugar. Mixed on medium and added flour slowly until heavy dough that wasn't sticky. Covered with wet towel to raise for 1.5 hr.

## Toppings
### Peppers
Coarsely chopped one yellow and one orange pepper, tossed in oil and salt, and put on sided baking sheet. Baked peppers while oven preheated to 400, plus about 20 minutes, stirring once. Peppers were lightly caramelized but still firm.
### Onions
Finely diced one small sweet onion.
### Spinach
Medium chopped roughly 1.5 cups of spinach
    
## Construction
Put roughly 2 tab olive oil in sided baking sheet, and then pressed dough to fill. Spread Meijer pizza sauce. Covered with 2/3 of spinach, forming a full layer, but I could still see sauce. Spread most of 2 cup bag of shredded mozzarella. Spread most of the onion, followed by remaining spinach. Removed peppers from oven and placed directly on pizza, forming a thick layer. Sprinkled crushed red peppers across pizza. Backed pizza for about 17 minutes at 400 degrees, until the edges were dark brown, cheeze was melted, and peppers were soft and had somewhat dried. Cooled on a wooden board (out of pan.)

## Result
Middle of crust was less crisp than hoped, but otherwise very good. Peppers and onions tasted great, and peppers had great texture, not too wet like previous attempts. Spinach was sufficient to be noticed, but was not stringy due to chopping.

### 4 stars.
